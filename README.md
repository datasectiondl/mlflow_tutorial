# MLFlow Tutorial

## Requirements
* Docker
* docker-compose
* Python (3.6.1 or higher)

## Step 0: Install libraries
```bash
pip install -r requirements.txt
```

## Step 1: Create annotation data
See [push_fetch](push_fetch)

## Step 2: Create recognition model
See [resnet](resnet)

## Step 3: Deploy recognition model
See [recognizer](recognizer)


## Liscense
Copyright 2017 Datasection Inc. Released under the MIT license.

This tutorial is created using open source software:  
TensorFlow

---
Copyright 2017 The TensorFlow Authors. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---
