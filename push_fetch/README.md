# Push/Fetch #

In this part, you can know below:
1. How to push (upload) your own data to the MLFLow
2. How to fetch (download) the pushed data from the MLFlow

In this tutorial, we'll create an sushi/not-sushi classifier.
Then we'll use image data here, but MLFlow can treat text data.

## Pushing data ##
After moved to ```push_fetch``` directory, run below command.

```
python push.py --stream_name <stream name> --token <public key of the MLFLow user> --image_dir <directory includes images>
```

If the command successfully done, you can see YYYYMMDDHHMMSS.csv file (let it call __evidence file__) under the current directory.

## Fetching data ##
For fetching, you can choose the way; "using evidence file" or "using stream name".

#### Using evidence file ####
```
python fetch.py --token <public key of the MLFLow user>  --output_dir <path to output directory> --evidence <evidence file...>
```
This way fetches
- not empty annotation data
- all image data listed in the evidence file　(Images without annotation data __will be__ downloaded)

#### Using stream name ####

```
python fetch.py --token <public key of the MLFLow user> --output_dir <path to output directory> --stream_name <a stream name>
```
This way fetches
- not empty annotation data
- Corresponding image data　(Images without annotation data __will not be__ downloaded)


## Exercise
```bash
$ pwd
# -> TUTORIAL_HOME/push_fetch

# Push the images
# "token" is the public key of the MLFlow user
$ python push.py --stream_name "***" --token "****" --image_dir images
# Check that a evidence CSV file (YYYYMMDDHHMMSS.csv) exists here.

# (Annotation at the MLFlow Console)

# Fetch the annotated images with the evidence file
$ python fetch.py --token "***" --output_dir fetched_images --evidence "YYYYMMDDHHMMSS.csv"

# Now, You can see the images and their annotation JSON files
# under the "fetched_images" directory!
```

## Using docker-compose command
```bash
$ docker network create mlflow
$ cd TUTORIAL_ROOT

# Push the images
$ docker-compose run --rm app python ./push_fetch/push.py --stream_name "***" --token "****" --image_dir ./push_fetch/images

# Fetch the annotated images with the evidence file
$ docker-compose run --rm app python ./push_fetch/fetch.py --token "***" --output_dir ./push_fetch/fetched_images --evidence "YYYYMMDDHHMMSS.csv"
```
