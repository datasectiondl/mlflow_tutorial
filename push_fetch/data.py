# -*- coding: utf-8 -*-

import base64


def new_image(path):
    print(path)
    enc_file = base64.b64encode(open(path, 'rb').read())
    return enc_file.decode('utf-8')
