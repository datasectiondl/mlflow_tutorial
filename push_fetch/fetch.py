# -*- coding: utf-8 -*-

import argparse
import csv
import os
import sys
import base64
import json
import ast
import requests
import raw_info
import payload
import annotation_info
from logzero import logger

COLUMN_NUM_MESSAGE_ID = 1
USE_FIXED = False
USE_MODIFIED = True
USE_DELETED = False
LIMIT = 5

RAW_SUFIX = '.jpg'
ANNOTATION_SUFIX = '_train.json'


def post(session, endpoint, data, headers):
    try:
        r = session.post(endpoint, data=data, headers=headers)
        r.raise_for_status()
    except requests.exceptions.HTTPError as e:
        logger.exception(str(e))
        message = ast.literal_eval(r.text)
        logger.error('Message from datamanager: {}\n'.format(message['message']))
        sys.exit(1)

    return r


def get_message_ids(session, manager_end_point, token, stream_name):
    annotation_endpoint = '%s/%s' % (manager_end_point, 'processed-data')
    headers = {'Token': token}
    ids = []
    offset = 0
    while True:
        info = annotation_info.new_stream_info(stream_name, USE_FIXED,
                                               USE_MODIFIED, USE_DELETED, LIMIT, offset)
        r = post(session, annotation_endpoint, data=info, headers=headers)
        messages = [res['message_id'] for res in json.loads(r.content)]
        if len(messages) == 0:
            break
        ids.extend(messages)
        if len(messages) < LIMIT:
            break
        offset += LIMIT

    return ids


def get_raw(session, manager_end_point, token, message_id):
    raw_endpoint = '%s/%s/%s' % (manager_end_point, 'raw', message_id)
    info = raw_info.new_raw_info(False, True)
    headers = {'Token': token}
    r = post(session, raw_endpoint, data=info, headers=headers)
    raw = payload.get_raw_from_payload(base64.b64decode(r.content))

    return base64.b64decode(raw)


def get_annotation(session, manager_end_point, token, stream_name, message_ids):
    annotation_endpoint = '%s/%s' % (manager_end_point, 'processed-data')
    headers = {'Token': token}
    annotations = []
    for i in range(0, len(message_ids), LIMIT):
        m_ids = message_ids[i:i + LIMIT]
        info = annotation_info.new_annotation_info(stream_name, m_ids, USE_FIXED, USE_MODIFIED, USE_DELETED, LIMIT)
        r = post(session, annotation_endpoint, data=info, headers=headers)
        annotations.extend(json.loads(r.content.decode('utf-8')))

    return annotations


def fetch_from_stream_name(manager_end_point, token, stream_name, without_raw, output_dir):
    logger.info('Stream name: %s' % stream_name)
    os.makedirs(output_dir, exist_ok=True)

    session = requests.Session()

    logger.info('Collecting id information of data to be fetched...')
    logger.info('(This may take several minutes if the number of data is large)')
    ids = get_message_ids(session, manager_end_point, token, stream_name)

    for i in ids:
        logger.info('Fetching: %s' % i)
        fetch_from_id(session, manager_end_point, token, i, without_raw, output_dir)

    logger.info('Fetching annotation data...')
    annotations = get_annotation(session, manager_end_point, token, stream_name, ids)
    for annotation in annotations:
        with open(os.path.join(output_dir, '%s%s' % (annotation['message_id'], ANNOTATION_SUFIX)), 'w') as a:
            a.write(annotation['train_metadata'])


def fetch_from_id(session, manager_end_point, token, id, without_raw, output_dir):
    if not without_raw:
        # Get raw data
        raw = get_raw(session, manager_end_point, token, id)
        with open(os.path.join(output_dir, '%s%s' % (id, RAW_SUFIX)), 'bw') as r:
            r.write(raw)


def fetch_from_ids(manager_end_point, token, paths, without_raw, output_dir):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    session = requests.Session()

    for path in paths:
        message_ids = []
        stream_name = ''
        with open(path, 'r') as f:
            reader = csv.reader(f)
            for i, row in enumerate(reader):
                if i == 0:
                    stream_name = row[1].split('.')[0]

                logger.info('Fetching: %s' % row[COLUMN_NUM_MESSAGE_ID])
                fetch_from_id(session, manager_end_point, token, row[COLUMN_NUM_MESSAGE_ID], without_raw, output_dir)
                message_ids.append(row[COLUMN_NUM_MESSAGE_ID])

        annotations = get_annotation(session, manager_end_point, token, stream_name, message_ids)
        for annotation in annotations:
            with open(os.path.join(output_dir, '%s%s' % (annotation['message_id'], ANNOTATION_SUFIX)), 'w') as a:
                a.write(annotation['train_metadata'])


def fetch(manager_end_point, token, stream_name, paths, without_raw, output_dir):
    if stream_name == '' or stream_name is None:
        fetch_from_ids(manager_end_point, token, paths, without_raw, output_dir)
    else:
        fetch_from_stream_name(manager_end_point, token, stream_name, without_raw, output_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MLFlow fetch python library.')
    parser.add_argument('--token', type=str, required=True,
                        help='public key of the MLFlow user')
    parser.add_argument('--output_dir', required=True, type=str,
                        help='output directory for downloaded raw data.')
    parser.add_argument('--evidence', metavar='', type=str, nargs='+',
                        help='file path of message_id to fetch data from MLFlow. '
                             'This option process ignore stream name.')
    parser.add_argument('--stream_name', type=str, help='stream name for downloading raw data')
    parser.add_argument('--datamanager', type=str, default='https://datamanager.mlflow.net',
                        help='end point of the datamanager')
    parser.add_argument('--without_raw', action='store_true',
                        help='If set, fetch only annotations without raw data.')
    args = parser.parse_args()

    logger.info('[MLFlow] Start fetch...')
    fetch(args.datamanager, args.token, args.stream_name, args.evidence, args.without_raw, args.output_dir)
    logger.info('[MLFlow] Finish fetch.')
