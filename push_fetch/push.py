# -*- coding: utf-8 -*-

import os
import argparse
import data
import payload
import requests
from datetime import datetime as dt
from logzero import logger

DEMO_PRIORITY = 0


def load(dir_name):
    files = os.listdir(dir_name)
    files.sort()
    return files


def post(session, receiver_end_point, stream_name, token, path):
    img_data = data.new_image(path)
    payload_data = payload.new_payload(img_data, path, DEMO_PRIORITY, stream_name)
    headers = {'Token': token, 'Content-Type': 'application/json'}
    r = session.post(receiver_end_point, data=payload_data, headers=headers)
    if r.status_code != 200:
        logger.info('[MLFlow] failed to push. code: %d, file: %s' % (r.status_code, path))
        return
    res = r.json()
    logger.info('[MLFlow] success to push. message_id: %s, file: %s' % (res['message_id'], path))
    return res['message_id']


def push(receiver_end_point, stream_name, token, dirs):
    now = dt.now()
    timestamp = now.strftime('%Y%m%d%H%M%S')
    # close automatically
    s = requests.Session()
    with open('%s.csv' % timestamp, 'w') as f:
        for dir_name in dirs:
            logger.info('[MLFlow] processing directory: %s' % dir_name)
            files = load(dir_name)
            for file in files:
                path = os.path.join(dir_name, file)
                r = post(s, receiver_end_point, stream_name, token, path)
                if r == '':
                    continue
                f.write('%s,%s\n' % (path, r))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MLFlow push python library.')
    parser.add_argument('--stream_name', type=str, required=True,
                        help='stream name')
    parser.add_argument('--token', type=str, required=True,
                        help='public key of the MLFlow user')
    parser.add_argument('--image_dir', type=str, nargs='+', required=True,
                        help='directory path of data to push MLFlow')
    parser.add_argument('--receiver', type=str, default='https://receiveapi.mlflow.net',
                        help='end point of the receiver')
    args = parser.parse_args()

    logger.info('[MLFlow] push starting...')
    push(args.receiver, args.stream_name, args.token, args.image_dir)
    logger.info('[MLFlow] push finish.')
