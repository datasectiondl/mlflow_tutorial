# Recognizer
Let's set up recognition server (we call this "recognizer") using the learned model.

## Exercise
```bash
$ pwd
# -> TUTORIAL_ROOT/recognizer

# Specify the model directory and port number to use.
$ vim .env

# Let's build docker image for the recognizer
$ docker-compose build
# Run the recognizer
$ docker-compose up
# Now, recognizer is up at localhost:8080/recognizer


# Let's throw an image to the recognizer and check the results!
# Open the another terminal
$ pwd
# -> TUTORIAL_ROOT/recognizer

$ python post_test_image.py \
  --recognizer http://localhost:8080/recognizer \
  --image ../push_fetch/images/sushi_01.jpg  \
  --category_json ../resnet/category_index.json
```

### Using docker-compose command
```bash
# Let's throw an image to the recognizer and check the results using docker.
# Open the another terminal
$ cd TUTORIAL_ROOT
# Attention: end point of the recognizer is different from not-docker version.
$ docker-compose run --rm app python ./recognizer/post_test_image.py --recognizer http://recognizer:8989/recognizer  --image ./push_fetch/images/sushi_01.jpg --category_json ./resnet/category_index.json
```
