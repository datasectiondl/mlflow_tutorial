# -*- coding: utf-8 -*-

import argparse
import os
from tqdm import tqdm


def main():
    input_dir = args.input_dir
    output_dir = args.output_dir
    exts = ('.jpg')

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    file_all = []
    for root, dirnames, filenames in os.walk(input_dir):
        for filename in filenames:
            if filename.endswith(exts):
                file_all.append(os.path.join(root, filename))

    for record in tqdm(file_all):
        pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_dir', default='input_dir', type=str, help='input dir path.')
    parser.add_argument('-o', '--output_dir', default='output_dir', type=str, help='output dir path.')
    parser.add_argument('-m', '--mode', default='age', type=str, help='age or gender')
    args = parser.parse_args()

    main()
