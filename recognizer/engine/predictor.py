# -*- coding: utf-8 -*-

import os
import numpy as np
from PIL import Image
import tensorflow as tf

# Force load contrib ops
dir(tf.contrib)


class Predictor(object):
    def __init__(self, model=None):
        print("Initializing the model...")
        self.batch_size = 1
        self.input_height = 224
        self.input_width = 224

        ckpt = tf.train.get_checkpoint_state(model)

        if ckpt and ckpt.model_checkpoint_path:
            print("Checkpoint path: %s" % ckpt.model_checkpoint_path)
            self.meta_graph = os.path.join("%s.meta" % ckpt.model_checkpoint_path)            
        else:
            print('No checkpoint file found.')
            return
        self.params = os.path.splitext(self.meta_graph)[0]
        self.gpu_allow_growth = True
        self.num_threads = 1

        self.sess = self._get_session()

        # Restore graph in the session
        _ = tf.train.import_meta_graph(self.meta_graph)
        saver = tf.train.Saver()
        # Restore the pre-trained parameters
        saver.restore(self.sess, self.params)

        graph = tf.get_default_graph()
        self.model_input = graph.get_tensor_by_name('input:0')
        self.predict_score = graph.get_tensor_by_name('predict_score:0')
        print("Initializing model is done.")

    def _get_session(self):
        gpu_options = tf.GPUOptions(allow_growth=self.gpu_allow_growth)

        return tf.Session(config=tf.ConfigProto(
            gpu_options=gpu_options, intra_op_parallelism_threads=self.num_threads))

    def predict(self, input_image, adjust_image=True):
        if adjust_image:
            input_image = self._adjust_image_size(input_image)

        predict_score_nd = self.sess.run(self.predict_score, feed_dict={self.model_input: input_image})
        self._prediction = int(predict_score_nd[0].argmax())

    def _adjust_image_size(self, rgb_img):
        rgb_img = Image.fromarray(rgb_img)
        width, height = rgb_img.size

        # Resize
        if (self.input_height, self.input_width) != (height, width):
            rgb_img = rgb_img.resize((self.input_height, self.input_width), Image.BICUBIC)

        rgb_img = np.asarray(rgb_img)

        # Expand (HWC) -> (NHWC)
        if len(rgb_img.shape) != 4:
            rgb_img = np.expand_dims(rgb_img, axis=0)

        return rgb_img

    @property
    def prediction(self):
        return self._prediction

    @prediction.setter
    def prediction(self, prediction):
        self._prediction = prediction
