#!/usr/bin/env python
# -*- coding: utf-8 -*-

from helper.image import Image
from helper.feature import Feature


class AnnotateImageRequest(object):
    def __init__(self, image, features):
        self.__image = image
        self.__features = features

    @property
    def image(self):
        return self.__image

    @image.setter
    def image(self, image):
        self.__image = image

    @property
    def features(self):
        return self.__features

    @features.setter
    def features(self, features):
        self.__features = features

    @staticmethod
    def serialize(dic):
        image, features = None, []
        if 'image' in dic:
            image = Image.serialize(dic['image'])
        if 'features' in dic:
            for f in dic['features']:
                feature = Feature.serialize(f)
                features.append(feature)
        return AnnotateImageRequest(image, features)
