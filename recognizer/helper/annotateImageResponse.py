#!/usr/bin/env python
# -*- coding: utf-8 -*-


class AnnotateImageResponse(object):
    def __init__(self, prediction=None, error=None):
        if prediction is not None:
            self.__prediction = prediction
        if error is not None:
            self.__error = error

    @property
    def prediction(self):
        return self.__prediction

    @prediction.setter
    def prediction(self, value):
        self.__prediction = value

    @prediction.deleter
    def prediction(self):
        del self.__prediction

    @property
    def error(self):
        return self.__error

    @error.setter
    def error(self, value):
        self.__error = value

    @error.deleter
    def error(self):
        del self.__error
