#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Feature(object):
    def __init__(self, _type, thresh=0.5, maxResult=-1):
        self.__type = _type
        self.__thresh = thresh
        self.__maxResult = maxResult

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, _type):
        self.__type = _type

    @property
    def thresh(self):
        return self.__thresh

    @thresh.setter
    def thresh(self, thresh):
        self.__thresh = thresh

    @property
    def maxResult(self):
        return self.__maxResult

    @maxResult.setter
    def maxResult(self, maxResult):
        self.__maxResult = maxResult

    @staticmethod
    def serialize(dic):
        _type = None
        thresh = 0.5
        maxResult = -1
        if 'type' in dic:
            _type = dic['type']
        if 'thresh' in dic:
            thresh = dic['thresh']
        if 'maxResult' in dic:
            maxResult = dic['maxResult']
        return Feature(_type, thresh, maxResult)
