#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Image(object):
    def __init__(self, content=None, url=None):
        if content:
            self.__content = content
        if url:
            self.__url = url

    @property
    def content(self):
        return self.__content

    @content.setter
    def content(self, content):
        self.__content = content

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        self.__url = url

    @staticmethod
    def serialize(dic):
        content, url = (None,)*2
        if 'content' in dic:
            content = dic['content']
        if 'url' in dic:
            url = dic['url']

        return Image(content, url)
