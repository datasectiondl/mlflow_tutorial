# -*- coding: utf-8 -*-

import base64
import numpy as np
from PIL import Image
from io import BytesIO
import urllib.request
import os
import ujson
import sys

# sys.path.append(os.pardir)
from helper.logs import MyLog

MODULE_NAME = 'image_helper'
MyLog.initialize('.', MODULE_NAME)


class InputError(Exception):
    def __init__(self, message):
        self.message = message


def convert(input_image):
    try:
        image_type = input_image.format

        if image_type not in ('JPEG', 'PNG', 'GIF'):
            err_msg = 'Unsupported image format (%s) is given. Supported formats are JPEG, PNG, and GIF.' \
                      % input_image.format
            raise InputError(err_msg)

        image = np.array(input_image)

        if len(image) in (2, 4):
            input_image = input_image.convert('RGB')
            image = np.array(input_image)

        return image, image_type

    except Exception as err:
        MyLog.write(MODULE_NAME, level='ERROR', message=err.message)
        raise err


def string2image(base64string):
    try:
        image = Image.open(BytesIO(base64.b64decode(base64string)))
        return convert(image)
    except Exception as err:
        MyLog.write(MODULE_NAME, level='ERROR', message=err.message)
        raise err


def url2image(url):
    try:
        raw = urllib.request.urlopen(url).read()
        raw_decode = base64.b64decode(raw.decode('utf-8'))
        body = ujson.loads(raw_decode)
        body_pay_dict = ujson.loads(body['payload'])
        image, image_type = string2image(body_pay_dict['data'])

        return image, image_type

    except Exception as err:
        MyLog.write(MODULE_NAME, level='ERROR', message=err)
        raise err
