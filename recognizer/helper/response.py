#!/usr/bin/env python
# -*- coding: utf-8 -*-

from helper.annotateImageResponse import AnnotateImageResponse


class Response(object):
    def __init__(self, responses=[]):
        self.__responses = responses

    @property
    def responses(self):
        return self.__responses

    @responses.setter
    def responses(self, responses):
        self.__responses = responses

    def add(self, annotateImageRes):
        self.__responses.append(annotateImageRes)
