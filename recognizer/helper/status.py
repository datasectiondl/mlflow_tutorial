#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Status(object):
    def __init__(self, code=None, message=None, details=None):
        if code:
            self.__code = code
        if message:
            self.__message = message
        if details:
            self.__details = details

    @property
    def code(self):
        return self.__code

    @code.setter
    def code(self, code):
        self.__code = code

    @property
    def message(self):
        return self.__message

    @message.setter
    def message(self, message):
        self.__message = message

    @property
    def details(self):
        return self.__details

    @details.setter
    def details(self, details):
        self.__details = details

    @staticmethod
    def serialize(dic):
        code, message, details = (None,)*3
        if 'code' in dic:
            code = dic['code']
        if 'message' in dic:
            message = dic['message']
        if 'details' in dic:
            details = dic['details']
        return Status(code, message, details)
