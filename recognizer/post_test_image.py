# -*- coding: utf-8 -*-

import argparse
import base64
import requests
import json
from logzero import logger


def main():
    img_path = args.image
    recog_end_point = args.recognizer

    enc_file = base64.b64encode(open(img_path, 'rb').read())
    enc_file = enc_file.decode('utf-8')

    data = {'requests': [{'image': {'content': enc_file},
                          'features': [{'type': 'CLASSIFICATION'}]}]}
    data = json.dumps(data)

    r = requests.post(recog_end_point, data=data)

    if r.status_code != 200:
        logger.error('Test failed. code: %d, file: %s' % (r.status_code, img_path))
        return

    res = r.json()
    [res_dict] = res['responses']
    pred = res_dict['prediction']

    if args.category_json is None:
        logger.info(pred)
    else:
        with open(args.category_json, 'r') as f:
            class_def = json.load(f)
        class_def_inv = {v: k for k, v in class_def.items()}
        logger.info(class_def_inv[int(*pred)])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--recognizer', type=str, required=True,
                        help='end point of the recognizer (e.g. http://localhost:8080/recognizer)')
    parser.add_argument('--image', type=str, required=True, help='path to the test image')
    parser.add_argument('--category_json', type=str, default=None,
                        help='The json file which includes category information')
    args = parser.parse_args()

    main()
