# -*- coding: utf-8 -*-

import tornado.ioloop
import tornado.web
import ujson
import argparse
from engine.predictor import Predictor
from helper.request import Request
from helper.status import Status
from helper.annotateImageResponse import AnnotateImageResponse
from helper.response import Response
from helper.imageHelper import string2image, url2image
from helper.logs import MyLog
from logzero import logger

MODULE_NAME = 'recognizer'
MyLog.initialize('.', MODULE_NAME)


class StatusHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_status(200)
        self.finish()
        return


class ServerHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', 'x-requested-with')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def get(self):
        MyLog.write(MODULE_NAME, level='INFO', message='Got GET.')
        self.set_status(200)
        self.finish()
        return

    def post(self):
        MyLog.write(MODULE_NAME, level='INFO', message='Got POST.')

        request_body = tornado.escape.json_decode(self.request.body)
        request_data = Request.serialize(request_body)

        response = Response([])
        for annotate_img_req in request_data.requests:
            annotate_img = annotate_img_req.image
            annotate_features = annotate_img_req.features
            content, url = (None,) * 2
            if hasattr(annotate_img, 'content'):
                content = annotate_img.content
            if hasattr(annotate_img, 'url'):
                url = annotate_img.url
            try:
                # Get image from url or request content
                if content:
                    image, image_type = string2image(content)
                else:
                    image, image_type = url2image(url)
            except Exception as err:
                status = Status(code=400, message=err.message)
                annotate_img_res = AnnotateImageResponse(error=status)
                response.add(annotate_img_res)
                MyLog.write(MODULE_NAME, level='ERROR', message=err.message)
                self.finish(ujson.dumps(response))
                return

            # Prediction
            for feature in annotate_features:
                if feature.type == 'CLASSIFICATION':
                    try:
                        predictor.predict(input_image=image)
                        annotate_img_res = AnnotateImageResponse(
                            prediction=[str(predictor.prediction)])
                        response.add(annotate_img_res)
                        MyLog.write(MODULE_NAME, level='INFO',
                                    message='Successfully predicted. (prediction: %s)' % predictor.prediction)
                    except Exception as err:
                        status = Status(code=500, message='Internal Server Error. Please contact admin.')
                        annotate_img_res = AnnotateImageResponse(error=status)
                        response.add(annotate_img_res)
                        MyLog.write(MODULE_NAME, 'Recognizer has problems! %s!' % format(err.message))
                        break
        self.finish(ujson.dumps(response))


def make_app():
    settings = {
        'debug': True,
        'autoreload': True
    }
    return tornado.web.Application([
        (r'/status', StatusHandler),
        (r'/recognizer', ServerHandler),
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': 'static'})
    ], **settings)


def get_args():
    parser = argparse.ArgumentParser(description='Recognizer args...')
    parser.add_argument('--moder_dir', type=str, required=True, help='path to the model directory')
    parser.add_argument('--port', type=int, required=True, help='port')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = get_args()

    # Load model
    predictor = Predictor(args.moder_dir)

    app = make_app()
    app.listen(args.port)

    logger.info('Server is up at %d...' % args.port)
    logger.info('If you run with docker, access to the \"HOST_PORT\" port which is written in the \".env\" file')
    tornado.ioloop.IOLoop.current().start()
