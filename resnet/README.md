# README #

In this part, we create a classification model (ResNet v2) for sushi/not-sushi problem.

## Exercise
```bash
$ pwd
# -> TUTORIAL_ROOT/resnet

# Create the necessary 3 files for training the model
#   1. JSON file which includes category-index information
#   2. Training CSV file
#   3. Validation CSV file
$ python create_csv_and_category_index.py \
  --extra_json ../push_fetch/extra_info.json \
  --fetched_dir ../push_fetch/fetched_images
# Now, You can find "category_index.json", "training.csv", "validation.csv"

# Check settings
# Here, we'll use default settings.
$ cat config.py

# Let's start train ResNet!
$ python trainer.py
```
While training, learned parameters are periodically saved in the ```RESULT_ROOT_DIR``` in the ```config.py```.


### Using docker-compose command

```
$ cd TUTORIAL_ROOT

# Create the necessary 3 files for training the model
#   1. JSON file which includes category-index information
#   2. Training CSV file
#   3. Validation CSV file
$ docker-compose run --rm app python ./resnet/create_csv_and_category_index.py \
  --extra_json ./push_fetch/extra_info.json \
  --fetched_dir ./push_fetch/fetched_images \
  --train_csv ./resnet/training.csv \
  --val_csv ./resnet/validation.csv \
  --output_json ./resnet/category_index.json
```
__Attention__: When using data not included in docker image, you need to be able to access data to use from docker image. (In this case, created train.csv and validataion.csv are empty)
Simplest way to solve this is moving the data under this tutorial directory, and re-building the docker image.

```
# Modify settings becasue the root directory is different from non docker version.
$ vim ./resnet/config.py
-> RESULT_ROOT_DIR = './recognizer/model'
-> TRAINING_CSV_FILE = './resnet/training.csv'
-> VAL_CSV_FILE = './resnet/validation.csv'

# Let's start train ResNet!
$ docker-compose run --rm app python ./resnet/trainer.py
```
