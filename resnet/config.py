# -*- coding: utf-8 -*-


class Config(object):
    GPU_ID = ''
    RESULT_ROOT_DIR = '../recognizer/model'
    BATCH_SIZE = 64
    NUM_CLASSES = 2
    IMAGE_SIZE = 224
    TRAINING_CSV_FILE = './training.csv'
    VAL_CSV_FILE = './validation.csv'
    DATA_ROOT_DIR = ''
    INITIAL_LEARNING_RATE = 1e-4
    MAX_EPOCH = 200
    VAL_INTERVAL = 10  # epoch
    SAVE_INTERVAL = 20  # epoch

    AUGMENTATION = True

    SAVE_TRAINING_IMAGE = True

    def __init__(self):
        pass

    def display(self, logger):
        """Display Configuration values."""
        logger.info('Configurations:')
        for a in dir(self):
            if not a.startswith('__') and not callable(getattr(self, a)):
                logger.info('{:30} {}'.format(a, getattr(self, a)))
