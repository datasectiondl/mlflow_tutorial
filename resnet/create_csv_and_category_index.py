# -*- coding: utf-8 -*-

import argparse
import os
import json
import csv
import random
from logzero import logger


def create_dir(file_path):
    dirname = os.path.dirname(file_path)
    if not os.path.exists(dirname) and dirname != '':
        os.makedirs(dirname)


def main():
    fetched_dir = args.fetched_dir
    train_csv = args.train_csv
    val_csv = args.val_csv
    image_exts = ('.jpg', '.jpeg', '.png')

    print('Fetched directory:', fetched_dir)

    create_dir(train_csv)
    create_dir(val_csv)

    file_all = []
    for root, dirnames, filenames in os.walk(fetched_dir):
        for filename in filenames:
            if filename.endswith(image_exts):
                file_all.append(os.path.abspath(os.path.join(root, filename)))

    random.shuffle(file_all)

    with open(args.extra_json, 'r') as f:
        tmp = json.load(f)
        class_name = list(tmp.keys())[0]
        categories = list(tmp[class_name].values())[0]

    # Create json file which has category-index information
    cate_dict = {}
    for i, category in enumerate(categories):
        cate_dict[category] = i

    with open(args.output_json, 'w') as f:
        json.dump(cate_dict, f)

    # Create training/validation csv files
    name_label = []
    counter = 0
    for image_name in file_all:
        json_file = os.path.splitext(image_name)[0] + '_train.json'
        try:
            with open(json_file, 'r') as f:
                dict = json.load(f)
        except Exception as e:
            logger.exception(e)
            continue

        counter += 1

        [label] = dict[class_name]
        label_id = cate_dict[label]
        name_label.append([counter, image_name, label_id])

    name_label.sort()
    # As default, 70 % is for training, 30 % is for validation
    train_num = int(len(name_label) * args.ratio_training_data)
    with open(train_csv, 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows(name_label[:train_num])
    with open(val_csv, 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows(name_label[train_num:])

    logger.info('Category index: %s' % args.output_json)
    logger.info('Training csv: %s' % train_csv)
    logger.info('Validation csv: %s'% val_csv)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fetched_dir', required=True,
                        type=str, help='path to the fetched directory')
    parser.add_argument('-e', '--extra_json', required=True,
                        type=str, help='path to the json which includes the class definition')
    parser.add_argument('-o', '--output_json', default='category_index.json',
                        type=str, help='path to the output json file')
    parser.add_argument('-t', '--train_csv', default='training.csv', type=str, help='path to the training csv file')
    parser.add_argument('-v', '--val_csv', default='validation.csv', type=str, help='path to the validation csv file')
    parser.add_argument('-r', '--ratio_training_data', default=0.7, type=float, help='how much use data for training')
    args = parser.parse_args()

    main()
