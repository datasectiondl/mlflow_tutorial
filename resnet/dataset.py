# encoding: utf-8

from math import ceil
import numpy as np
import tensorflow as tf
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import random_ops
from tensorflow.python.ops import control_flow_ops


class InputSupplier(object):
    def __init__(self, csv_file, shuffle, data_root_dir, augmentation, batch_size, allow_smaller_final_batch,
                 img_size=224, logger=None):
        self.logger = logger
        self.data_root = data_root_dir
        self.image_height = img_size
        self.image_width = img_size
        # TODO: support png image
        self.input_image_type = 'jpg'
        self.num_threads = 4
        self.csv_col_num = 3
        self.augmentation = augmentation
        self.allow_smaller_final_batch = allow_smaller_final_batch
        self.batch_size = batch_size
        self.csv = csv_file
        self.record_num = len(np.loadtxt(self.csv, delimiter=',', dtype=str))
        self.shuffle = shuffle
        self.max_iteration = None

        # Set max iteration number
        self.set_iteration_num(self.record_num)

    def set_iteration_num(self, whole_recode_num):
        if self.batch_size != 1:
            self.max_iteration = int(ceil(whole_recode_num / self.batch_size))
        else:
            self.max_iteration = int(whole_recode_num)

    def read_csv(self, filename_queue):
        reader = tf.TextLineReader()
        key, serialized_example = reader.read(filename_queue)

        input_id, input_path, label = tf.decode_csv(serialized_example, [[0], ['input'], [0]])
        input_path = tf.string_join([self.data_root, input_path])
        return input_id, input_path, label

    def read_image_from_path(self, path):
        if self.input_image_type == 'jpg' or self.input_image_type == 'jpg':
            jpg = tf.read_file(path)
            image = tf.image.decode_jpeg(jpg, channels=3, dct_method='INTEGER_ACCURATE')
        elif self.input_image_type == 'png':
            png = tf.read_file(path)
            image = tf.image.decode_png(png, channels=3)
        else:
            self.logger.error('%s type is not supported as input. Supported type is %s, and %s.' % (
                self.input_image_type, 'png', 'jpg'))

        resize_method = tf.image.ResizeMethod.BILINEAR

        image = tf.image.resize_images(image, [self.image_height, self.image_width], method=resize_method)

        if self.augmentation:
            image = self.do_augmentation(image)

        image = tf.clip_by_value(image, 0, 255)

        return image

    def input_pipeline(self):
        filename_queue = tf.train.string_input_producer([self.csv], shuffle=self.shuffle)
        # Read all file paths & create shuffled mini-batch on file paths
        index, path, label = self.read_csv(filename_queue)

        index_batch, path_batch, label_batch = tf.train.shuffle_batch(
            [index, path, label],
            batch_size=1,
            min_after_dequeue=self.record_num,
            capacity=self.record_num + (self.num_threads + 3),
            num_threads=self.num_threads
        )

        index_batch_flatten = tf.reshape(index_batch, [-1])
        path_batch_flatten = tf.reshape(path_batch, [-1])
        age_label_batch_flatten = tf.reshape(label_batch, [-1])
        image_batch_flatten = tf.map_fn(self.read_image_from_path, path_batch_flatten, tf.float32)

        # Create non shuffled mini-batch on images using shuffled file paths
        index_batch, path_batch, image_batch, label_batch = tf.train.batch(
            [index_batch_flatten, path_batch_flatten, image_batch_flatten, age_label_batch_flatten],
            batch_size=self.batch_size,
            capacity=32 + (self.num_threads + 3) * self.batch_size,
            num_threads=self.num_threads,
            allow_smaller_final_batch=self.allow_smaller_final_batch
        )

        index_batch = tf.squeeze(index_batch, axis=1)
        path_batch = tf.squeeze(path_batch, axis=1)
        image_batch = tf.squeeze(image_batch, axis=1)
        label_batch = tf.squeeze(label_batch, axis=1)

        tf.summary.histogram('input_ids', index_batch)
        tf.summary.histogram('labels', label_batch)

        return index_batch, path_batch, image_batch, label_batch

    def do_augmentation(self, img):
        # Geometrical augmentation
        img = tf.image.random_flip_left_right(img)
        img = self.randomly_apply_func(img, self.resize, 0.3)
        img = self.randomly_apply_func(img, self.rotation, 0.3)
        img = self.randomly_apply_func(img, self.crop, 0.3)

        # Augmentation on color
        img = tf.image.random_brightness(img, max_delta=50)
        img = tf.image.random_contrast(img, lower=0.8, upper=1.2)
        img = tf.image.random_saturation(img, lower=0.6, upper=1.2)

        return img

    def randomly_apply_func(self, img, f, prob=0.5):
        uniform_random = random_ops.random_uniform([], 0, 1.0)
        cond = math_ops.less(uniform_random, prob)

        img = control_flow_ops.cond(cond,
                                    lambda: f(img),
                                    lambda: img)

        return img

    def resize(self, img, low=0.8, high=1.2):
        resize_ratio = np.random.uniform(low=low, high=high)
        img = tf.image.resize_images(img, (int(self.image_height * resize_ratio), int(self.image_width * resize_ratio)))
        img = tf.image.resize_images(img, (self.image_height, self.image_width), method=tf.image.ResizeMethod.BILINEAR)

        return img

    def rotation(self, img):
        random_angle = random_ops.random_normal([], mean=0, stddev=0.2)
        img = tf.contrib.image.rotate(img, angles=random_angle, interpolation='BILINEAR')

        return img

    def crop(self, img):
        crop_ratio = np.random.uniform(0.8, 1.0)

        height, width = int(self.image_height * crop_ratio), int(self.image_width * crop_ratio)
        img = tf.random_crop(img, [height, width, 3])
        img = tf.image.resize_images(img, (self.image_height, self.image_width), method=tf.image.ResizeMethod.BILINEAR)

        return img
