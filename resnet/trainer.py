# encoding: utf-8

import os
import tensorflow as tf
from logzero import logger

from config import Config
from dataset import InputSupplier
from resnet import resnet_model


def train():
    graph = tf.Graph()
    with graph.as_default():
        sess_config = tf.ConfigProto()
        sess_config.gpu_options.allow_growth = True

        with tf.Session(config=sess_config) as sess:
            training_input_supplier = InputSupplier(csv_file=config.TRAINING_CSV_FILE,
                                                    data_root_dir=config.DATA_ROOT_DIR,
                                                    augmentation=config.AUGMENTATION,
                                                    batch_size=config.BATCH_SIZE,
                                                    shuffle=True,
                                                    allow_smaller_final_batch=False,
                                                    logger=logger)

            val_input_supplier = InputSupplier(csv_file=config.VAL_CSV_FILE,
                                               batch_size=1,
                                               data_root_dir=config.DATA_ROOT_DIR,
                                               augmentation=False,
                                               shuffle=False,
                                               allow_smaller_final_batch=True,
                                               logger=logger)

            ##################
            # Training graph #
            ##################
            with tf.name_scope('input_training'):
                _, inputs_train_names, inputs_train, labels_train = \
                    training_input_supplier.input_pipeline()

                tf.summary.image('input_image', inputs_train)

            logits_train, logits_prob_train, prediction_train = resnet_model(inputs=inputs_train,
                                                                             num_classes=config.NUM_CLASSES,
                                                                             is_training=True,
                                                                             reuse=False)

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                cross_entropy_loss = tf.reduce_mean((tf.nn.sparse_softmax_cross_entropy_with_logits(
                    logits=logits_train, labels=labels_train)))
                train_regularization_loss = 1e-3 * tf.add_n(
                    [tf.nn.l2_loss(v) for v in tf.trainable_variables()])
                total_train_loss = cross_entropy_loss + train_regularization_loss

            tf.summary.scalar('Losses/total_train_loss', total_train_loss)

            recall_train, recall_train_op = tf.metrics.recall(labels=labels_train, predictions=prediction_train)
            precision_train, precision_train_op = tf.metrics.precision(labels=labels_train,
                                                                       predictions=prediction_train)
            with tf.control_dependencies([recall_train_op, precision_train_op]):
                recall_train = (2 * recall_train * precision_train) / (recall_train + precision_train)
                tf.summary.scalar('Scores/train/recall', tf.reduce_mean(recall_train))
                precision_train = (2 * precision_train * precision_train) / (precision_train + precision_train)
                tf.summary.scalar('Scores/train/precision', tf.reduce_mean(precision_train))
                f1_train = (2 * recall_train * precision_train) / (recall_train + precision_train)
                tf.summary.scalar('Scores/train/f1', tf.reduce_mean(f1_train))

            ####################
            # Validation graph #
            ####################
            with tf.name_scope('input_validation'):
                _, inputs_val_names, inputs_val, labels_val = val_input_supplier.input_pipeline()

            # For deploy, give an intuitive name to input tensor
            inputs_val = tf.identity(inputs_val, 'input')

            logits_val, logits_prob_val, prediction_val = resnet_model(inputs=inputs_val,
                                                                       num_classes=config.NUM_CLASSES,
                                                                       is_training=False,
                                                                       reuse=True)

            # For deploy, give an intuitive name to output tensor
            logits_prob_val = tf.identity(logits_prob_val, 'predict_score')

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                cross_entropy_loss = tf.reduce_mean((tf.nn.sparse_softmax_cross_entropy_with_logits(
                    logits=logits_val, labels=labels_val)))
                val_regularization_loss = 1e-3 * tf.add_n(
                    [tf.nn.l2_loss(v) for v in tf.trainable_variables()])
                total_val_loss = cross_entropy_loss + val_regularization_loss

            tf.summary.scalar('Losses/total_val_loss', total_val_loss)

            recall_val, recall_val_op = tf.metrics.recall(labels=labels_val, predictions=prediction_val)
            precision_val, precision_val_op = tf.metrics.precision(labels=labels_val,
                                                                   predictions=prediction_val)
            with tf.control_dependencies([recall_val_op, precision_val_op]):
                recall_val = (2 * recall_val * precision_val) / (recall_val + precision_val)
                tf.summary.scalar('Scores/val/recall', tf.reduce_mean(recall_val))
                precision_val = (2 * precision_val * precision_val) / (precision_val + precision_val)
                tf.summary.scalar('Scores/val/precision', tf.reduce_mean(precision_val))
                f1_val = (2 * recall_val * precision_val) / (recall_val + precision_val)
                tf.summary.scalar('Scores/val/f1', tf.reduce_mean(f1_val))

            # Optimizer
            global_step = tf.Variable(0, dtype=tf.int32, trainable=False)
            with tf.control_dependencies([total_train_loss]):
                opt = tf.train.MomentumOptimizer(learning_rate=config.INITIAL_LEARNING_RATE, momentum=0.9)
                train_op = opt.minimize(total_train_loss, global_step=global_step)

            logger.info('Initializing all parameters before restoring. This will take a few minutes.')
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())

            # Saver
            # All parameters will be save for safety.
            saver_all = tf.train.Saver(tf.global_variables(), max_to_keep=0)

            # Summary writer
            all_summary_op = tf.summary.merge_all()
            summary_writer = tf.summary.FileWriter(os.path.join(config.RESULT_ROOT_DIR, 'tb_dir'), sess.graph)

            max_training_iteration = training_input_supplier.max_iteration
            max_val_iteration = val_input_supplier.max_iteration

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess, coord)
            initial_loss = sess.run(total_train_loss)
            logger.info('Initial loss before training: %f' % initial_loss)
            logger.info('Start training.')
            for epoch in range(config.MAX_EPOCH):
                # Training loop
                for iteration in range(max_training_iteration):
                    _, inputs_train_nd, inputs_train_names_nd, total_train_loss_nd, summary = \
                        sess.run([train_op, inputs_train, inputs_train_names, total_train_loss, all_summary_op])

                    logger.debug("%d/%d[epoch] %d/%d[iteration] training loss: %f" % (
                        epoch, config.MAX_EPOCH, iteration + 1, max_training_iteration, total_train_loss_nd))

                    summary_writer.add_summary(summary, global_step=epoch)

                # Validation loop
                if epoch % config.VAL_INTERVAL == 0 or (epoch + 1) == config.MAX_EPOCH:
                    for iteration in range(max_val_iteration):
                        inputs_val_nd, inputs_val_names_nd, total_val_loss_nd, summary, \
                        predicts_val_argmax_nd, labels_val_nd = \
                            sess.run([inputs_val, inputs_val_names,
                                      total_val_loss, all_summary_op,
                                      prediction_val, labels_val])

                        logger.debug("%d/%d[epoch] %d/%d[iteration] val loss: %f" % (
                            epoch, config.MAX_EPOCH, iteration + 1, max_val_iteration, total_val_loss_nd))

                # Save parameters
                if epoch % config.SAVE_INTERVAL == 0 or (epoch + 1) == config.MAX_EPOCH:
                    logger.debug('Saving parameters.')
                    save_dir = os.path.join(config.RESULT_ROOT_DIR)
                    os.makedirs(save_dir, exist_ok=True)

                    save_name = os.path.join(save_dir, 'epoch%06d.ckpt' % (epoch))
                    logger.info('Saving parameters as %s' % save_name)
                    saver_all.save(sess, save_name, global_step=epoch)
                    # Save protobuffer file
                    pb_dir, pb_name = os.path.split(save_name)
                    pb_name = pb_name.replace('ckpt', 'pb')
                    tf.train.write_graph(sess.graph_def, pb_dir, pb_name, as_text=True)

            coord.request_stop()
            coord.join(threads)


if __name__ == '__main__':
    config = Config()
    config.display(logger=logger)

    os.environ['CUDA_VISIBLE_DEVICES'] = config.GPU_ID

    os.makedirs(config.RESULT_ROOT_DIR, exist_ok=True)

    train()
